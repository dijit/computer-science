# Computer Science - Extra Resources

## Talks ##

- [Programming Paradigms](#programming-paradigms)
- [Miscellaneous](#miscellaneous)

### Programming Paradigms ###

- [Why Getters-and-Setters Is An Anti-Pattern? - Yegor Bugayenko](https://youtu.be/WSgP85kr6eU)

### Miscellaneous ###

- [2013 SouthEast LinuxFest - Richard Hipp - Checklists For Better Software](https://youtu.be/B9pulMZwUUY)
- [Reinventing Organizations](https://youtu.be/gcS04BI2sbk)